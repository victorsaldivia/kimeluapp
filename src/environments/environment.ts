// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
    apiKey: "AIzaSyBpCLz04ShELDQdFMtAMxU-yDu2Hc-c_tg",
    authDomain: "db-app2.firebaseapp.com",
    databaseURL: "https://db-app2.firebaseio.com",
    projectId: "db-app2",
    storageBucket: "db-app2.appspot.com",
    messagingSenderId: "553312808382"
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
